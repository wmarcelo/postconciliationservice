package pe.com.entel.hss.util;

import java.io.Closeable;
import java.io.IOException;
import java.util.NoSuchElementException;

import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class TelnetClientHandlerPool implements Closeable {

    private static Logger logger = Logger.getLogger(TelnetClientHandlerPool.class);

    public static final byte WHEN_EXHAUSTED_FAIL = GenericObjectPool.WHEN_EXHAUSTED_FAIL;
    public static final byte WHEN_EXHAUSTED_BLOCK = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    public static final byte WHEN_EXHAUSTED_GROW = GenericObjectPool.WHEN_EXHAUSTED_GROW;

    private ObjectPool pool;

    public TelnetClientHandlerPool(final TelnetClientHandler.TelnetClientHandlerBuilder builder,
                                   final int maxActive) {
        pool = new GenericObjectPool(new PoolableObjectFactory() {

            public void destroyObject(Object obj) throws Exception {
                if (obj instanceof TelnetClientHandler) {
                    ((TelnetClientHandler) obj).disconnect();
                }
            }

            public boolean validateObject(Object obj) {
                if (obj instanceof TelnetClientHandler) {
                    TelnetClientHandler client = (TelnetClientHandler) obj;
                    return client.isConnected() && client.isCmdQueueEmpty();
                }
                return false;
            }

            public Object makeObject() throws Exception {
                logger.info("Entro aqui!!");
                TelnetClientHandler client = builder.createTelnetClientHandler();
                Thread thread = new Thread(client);
                thread.start();
                return client;
            }

            public void activateObject(Object obj) throws Exception {
                // do nothing
            }

            public void passivateObject(Object obj) throws Exception {
                // do nothing
            }
        },
                maxActive);
    }

    public TelnetClientHandler get() throws Exception, NoSuchElementException, IllegalStateException {
        TelnetClientHandler c = (TelnetClientHandler) pool.borrowObject();
        logger.debug("Obteniendo cliente telnet -> " + c);
        return c;
    }

    public void putback(TelnetClientHandler c) throws Exception {
        logger.debug("Retornando al pool cliente telnet -> " + c);
        pool.returnObject(c);
    }

    public void remove(TelnetClientHandler c) throws Exception {
        logger.debug("Removiendo del pool cliente telnet -> " + c);
        pool.invalidateObject(c);
    }

    public void close() throws IOException {
        try {
            logger.debug("Cerrando pool de clientes telnet");
            pool.clear();
            pool.close();
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }
    }

}

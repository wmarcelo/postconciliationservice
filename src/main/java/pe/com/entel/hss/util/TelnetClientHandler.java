package pe.com.entel.hss.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class TelnetClientHandler extends JDriver implements Runnable {

    private static Logger logger = Logger.getLogger(TelnetClientHandler.class);

    private Socket socket = null;
    private InputStream inputStream = null;
    private InputStreamReader inputStreamReader = null;
    private BufferedOutputStream bos = null;
    private PrintWriter printWritter = null;
    private boolean commandOk;
    private boolean logged;

    private String host;
    private int port;
    private int timeoutRead;
    private int timeoutConnect;

    private String loginCmd;
    private String loginVal;
    private String loginErr;

    private String logoutCmd;
    private String logoutVal;
    private String logoutErr;

    private String cmdVal;
    private String cmdErr;
    private String cmdEnd;

    private LinkedHashSet<String> queue;

    public TelnetClientHandler(String host, int port, int timeoutRead, int timeoutConnect) {
        this.host = host;
        this.port = port;
        this.timeoutRead = timeoutRead;
        this.timeoutConnect = timeoutConnect;
        queue = new LinkedHashSet<String>();
    }

    public void open() throws Exception {
        this.socket = new Socket();
        this.socket.connect(new InetSocketAddress(host, port), timeoutConnect);
        this.socket.setSoTimeout(timeoutRead);
        this.socket.setKeepAlive(true);
        logger.debug("Instancia socket: " + this.socket);
        this.inputStream = this.socket.getInputStream();
        this.inputStreamReader = new InputStreamReader(this.inputStream);
        this.brResponse = new BufferedReader(this.inputStreamReader);
        this.bos = new BufferedOutputStream(this.socket.getOutputStream());
        this.printWritter = new PrintWriter(this.bos, true);

        if (loginCmd != null && !loginCmd.isEmpty()) {
            logger.debug("Iniciando login ...");
            sendCmdLogin(loginCmd, loginVal, loginErr);
            logged = true;
            resetValues();
            logger.info("Logueado!");
        }
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public boolean isCmdQueueEmpty() {
        return queue.isEmpty();
    }

    public boolean isLogged() {
        return logged;
    }

    private synchronized Map<String, Object> sendCmd(String cmd, String cmdVal, String cmdErr) throws Exception {
        try {
            logger.info("Ejecutando comando -> [ " + cmd + " ]");
            this.printWritter.println(cmd);
            this.printWritter.flush();
            return readCmdResponse(cmdVal, cmdErr);
        } finally {
            logger.debug("Se envio respuesta");
        }
    }

    private void sendCmdLogin(String cmd, String cmdVal, String cmdErr) throws Exception {
        InputStreamReader isr = new InputStreamReader(this.socket.getInputStream());
        BufferedReader brResp = new BufferedReader(isr);
        BufferedOutputStream bos = new BufferedOutputStream(this.socket.getOutputStream());
        PrintWriter pwriter = new PrintWriter(bos, true);
        String strLine;
        StringBuffer cmdResponseStr = new StringBuffer();

        try {

            logger.info("Ejecutando comando LOGIN -> [ " + cmd + " ]");
            pwriter.println(cmd);
            pwriter.flush();

            readResponseLoop:
            while ((strLine = brResp.readLine()) != null) {
                cmdResponseStr.append(strLine).append("\n");
                if (strLine.endsWith(cmdEnd)) {
                    logger.debug("Se encontro patron de terminado " + cmdEnd);
                    break readResponseLoop;
                }
            }

            logger.info("Respuesta ->  " + cmdResponseStr.toString());

            if (StringUtils.contains(cmdResponseStr.toString(), cmdVal)) {
                this.logged = true;
                logger.debug("Login Ok!");
            } else if (cmdResponseStr.toString().contains(cmdErr)) {
                this.logged = false;
                logger.debug("Login NoOk!");
            } else {
                this.logged = false;
                logger.debug("Login patron rstpa NO reconocido");
            }
        } finally {
//            try {
//                if (isr != null) {
//                    isr.close();
//                }
//            } catch (Exception ignoredException) {
//            }
//            try {
//                if (brResp != null) {
//                    brResp.close();
//                }
//            } catch (Exception ignoredException) {
//            }
//            try {
//                if (bos != null) {
//                    bos.close();
//                }
//            } catch (Exception ignoredException) {
//            }
//            try {
//                if (pwriter != null) {
//                    pwriter.close();
//                }
//            } catch (Exception ignoredException) {
//            }
        }

    }

    private void sendCmdLogout(String cmd, String cmdVal, String cmdErr) throws Exception {
        InputStreamReader isr = new InputStreamReader(this.socket.getInputStream());
        BufferedReader brResp = new BufferedReader(isr);
        BufferedOutputStream bos = new BufferedOutputStream(this.socket.getOutputStream());
        PrintWriter pwriter = new PrintWriter(bos, true);
        String strLine;
        StringBuffer cmdResponseStr = new StringBuffer();

        try {

            logger.info("Ejecutando comando LOGOUT -> [ " + cmd + " ]");
            pwriter.println(cmd);
            pwriter.flush();

            readResponseLoop:
            while ((strLine = brResp.readLine()) != null) {
                cmdResponseStr.append(strLine).append("\n");
                if (strLine.endsWith(cmdEnd)) {
                    logger.debug("Se encontro patron de terminado " + cmdEnd);
                    break readResponseLoop;
                }
            }

            logger.info("Respuesta ->  " + cmdResponseStr.toString());

            if (StringUtils.contains(cmdResponseStr.toString(), cmdVal)) {
                this.logged = false;
                logger.debug("Logout Ok!");
            } else if (StringUtils.contains(cmdResponseStr.toString(), cmdErr)) {
                this.logged = true;
                logger.debug("Logout NoOk!");
            } else {
                this.logged = true;
                logger.debug("Logout patron rstpa NO reconocido");
            }
        } finally {
//            try {
//                if (isr != null) {
//                    isr.close();
//                }
//            } catch (Exception ignoredException) {
//            }
//            try {
//                if (brResp != null) {
//                    brResp.close();
//                }
//            } catch (Exception ignoredException) {
//            }
//            try {
//                if (bos != null) {
//                    bos.close();
//                }
//            } catch (Exception ignoredException) {
//            }
//            try {
//                if (pwriter != null) {
//                    pwriter.close();
//                }
//            } catch (Exception ignoredException) {
//            }
        }

    }

    private synchronized Map<String, Object> readCmdResponse(String cmdVal, String cmdErr) throws Exception {
        //clearStbCmdResponse();
        Map<String, Object> result = new HashMap<String, Object>();
        String strLine;
        Boolean commandOk = false;
        StringBuffer stbCmdResponse = new StringBuffer();
        readResponseLoop:
        while ((strLine = this.brResponse.readLine()) != null) {
            stbCmdResponse.append(strLine).append("\n");
            //logger.debug("strLine: " + strLine);
            if (strLine.endsWith(cmdEnd)) {
                logger.debug("Se encontro patron de terminado");
                break readResponseLoop;
            }
        }

        logger.info("Respuesta ->  " + stbCmdResponse.toString());

        if (StringUtils.contains(stbCmdResponse.toString(), cmdVal)) {
            commandOk = true;
            logger.debug("Command Ok!");
        } else if (StringUtils.contains(stbCmdResponse.toString(), cmdErr)) {
            commandOk = false;
            logger.debug("Command NoOk");
        } else {
            commandOk = false;
            logger.debug("Patron rstpa NO reconocido: " + commandOk);
        }

        result.put("isOk", commandOk);
        result.put("resp", stbCmdResponse.toString());

        return result;
    }

    private void cleanCmdResponse() {
        if (this.stbCmdResponse != null) {
            clearStbCmdResponse();
        }
    }

    private void logout() throws Exception {
        if (logoutCmd == null || logoutCmd.isEmpty()) {
            logger.debug("Comando logout es nulo");
            logger.warn("No se ha realizado logout!");
            return;
        }

        logger.info("Iniciando logout ...");
        sendCmdLogout(logoutCmd, logoutVal, logoutErr);
        resetValues();
        logged = false;
        logger.info("Deslogueado!");
    }

    public void disconnect() throws Exception {

        cleanCmdResponse();

        logout();

        logger.info("Cerrando conexion con el puerto: " + this.socket);

        try {
            if (this.inputStream != null) {
                this.inputStream.close();
            }
        } catch (Exception ignoredException) {
        }

        try {
            if (this.inputStreamReader != null) {
                this.inputStreamReader.close();
            }
        } catch (Exception ignoredException) {
        }

        try {
            if (this.bos != null) {
                this.bos.close();
            }
        } catch (Exception ignoredException) {
        }

        try {
            if (this.printWritter != null) {
                this.printWritter.close();
            }
        } catch (Exception ignoredException) {
        }

        try {
            if ((this.socket != null) && (this.socket.isConnected())) {
                this.socket.close();
                this.socket = null;
            }
        } catch (Exception ignoredException) {
        }

        logger.debug("Conexion cerrada");

    }

    public synchronized Map<String, Object> execCmd(String cmd) throws Exception {
        queue.add(cmd);
        try {
            return sendCmd(cmd, cmdVal, cmdErr);
        } finally {
            queue.remove(cmd);
        }
    }

    private void resetValues() {
        cleanCmdResponse();
        commandOk = false;
    }

    @Override
    public void run() {
        try {
            open();
        } catch (Exception e) {
            logger.error(e);
        }
    }


    public static class TelnetClientHandlerBuilder {

        private String host;
        private int port;
        private int timeoutRead;
        private int timeoutConnect;
        private String loginCmd;
        private String loginVal;
        private String loginErr;
        private String logoutCmd;
        private String logoutVal;
        private String logoutErr;
        private String login2Cmd;
        private String login3Cmd;
        private String logout2Cmd;
        private String logout3Cmd;

        public TelnetClientHandlerBuilder() {
        }

        public TelnetClientHandlerBuilder host(final String host) {
            this.host = host;
            return this;
        }

        public TelnetClientHandlerBuilder port(final int port) {
            this.port = port;
            return this;
        }

        public TelnetClientHandlerBuilder timeoutRead(final int timeoutRead) {
            this.timeoutRead = timeoutRead;
            return this;
        }

        public TelnetClientHandlerBuilder timeoutConnect(final int timeoutConnect) {
            this.timeoutConnect = timeoutConnect;
            return this;
        }

        public TelnetClientHandlerBuilder loginCmd(final String loginCmd) {
            this.loginCmd = loginCmd;
            return this;
        }

        public TelnetClientHandlerBuilder loginVal(final String loginVal) {
            this.loginVal = loginVal;
            return this;
        }

        public TelnetClientHandlerBuilder loginErr(final String loginErr) {
            this.loginErr = loginErr;
            return this;
        }

        public TelnetClientHandlerBuilder logoutCmd(final String logoutCmd) {
            this.logoutCmd = logoutCmd;
            return this;
        }

        public TelnetClientHandlerBuilder logoutVal(final String logoutVal) {
            this.logoutVal = logoutVal;
            return this;
        }

        public TelnetClientHandlerBuilder logoutErr(final String logoutErr) {
            this.logoutErr = logoutErr;
            return this;
        }

        public TelnetClientHandlerBuilder login2Cmd(final String login2Cmd) {
            this.login2Cmd = login2Cmd;
            return this;
        }

        public TelnetClientHandlerBuilder login3Cmd(final String login3Cmd) {
            this.login3Cmd = login3Cmd;
            return this;
        }

        public TelnetClientHandlerBuilder logout2Cmd(final String logout2Cmd) {
            this.logout2Cmd = logout2Cmd;
            return this;
        }

        public TelnetClientHandlerBuilder logout3Cmd(final String logout3Cmd) {
            this.logout3Cmd = logout3Cmd;
            return this;
        }

        public TelnetClientHandler createTelnetClientHandler() {
            TelnetClientHandler t = new TelnetClientHandler(host, port, timeoutRead, timeoutConnect);
            t.setLoginCmd(loginCmd);
            t.setLoginVal(loginVal);
            t.setLoginErr(loginErr);
            t.setLogoutCmd(logoutCmd);
            t.setLogoutVal(logoutVal);
            t.setLogoutErr(logoutErr);
            return t;
        }

    }

    public void setLoginCmd(String loginCmd) {
        this.loginCmd = loginCmd;
    }

    public void setLoginVal(String loginVal) {
        this.loginVal = loginVal;
    }

    public void setLoginErr(String loginErr) {
        this.loginErr = loginErr;
    }

    public void setLogoutCmd(String logoutCmd) {
        this.logoutCmd = logoutCmd;
    }

    public void setLogoutVal(String logoutVal) {
        this.logoutVal = logoutVal;
    }

    public void setLogoutErr(String logoutErr) {
        this.logoutErr = logoutErr;
    }

    public void setCmdVal(String cmdVal) {
        this.cmdVal = cmdVal;
    }

    public void setCmdErr(String cmdErr) {
        this.cmdErr = cmdErr;
    }

    public void setCmdEnd(String cmdEnd) {
        this.cmdEnd = cmdEnd;
    }

    public boolean isCommandOk() {
        return commandOk;
    }
}

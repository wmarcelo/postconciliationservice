package pe.com.entel.hss.util;

import java.io.BufferedReader;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class JDriver {

    protected StringBuffer stbCmdResponse = null;
    protected BufferedReader brResponse = null;

    public JDriver() {
        this.stbCmdResponse = new StringBuffer();
    }

    void clearStbCmdResponse() {
        this.stbCmdResponse.setLength(0);
    }

    public StringBuffer getStbCmdResponse() {
        return this.stbCmdResponse;
    }

}

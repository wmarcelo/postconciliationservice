package pe.com.entel.hss.mapper;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import pe.com.entel.hss.domain.FrsHssRequest;
import pe.com.entel.hss.util.DateUtil;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class FrsHssRequestMapper implements RowMapper<FrsHssRequest> {

    private Logger logger = Logger.getLogger(FrsHssRequestMapper.class);

    @Override
    public FrsHssRequest mapRow(ResultSet rs, int i) throws SQLException {
        logger.debug("apply rowmapper FrsHssRequest");

        FrsHssRequest o = new FrsHssRequest();
        o.setNumHssRequestId(rs.getInt("NUM_HSS_REQUEST_ID"));
        o.setNumIdEirHistoricBl(rs.getInt("NUM_ID_EIR_HISTORIC_BL"));
        o.setFecModEirHistoricBl(DateUtil.convertDate(rs.getDate("FECMOD_EIR_HISTORIC_BL")));
        o.setVchHssRequestImei(rs.getString("VCH_HSS_REQUEST_IMEI"));
        o.setVchHssRequestImsi(rs.getString("VCH_HSS_REQUEST_IMSI"));
        o.setFecHssRequestEntdate(DateUtil.convertDate(rs.getDate("FEC_HSS_REQUEST_ENTDATE")));
        o.setFecHssRequestUpddate(DateUtil.convertDate(rs.getDate("FEC_HSS_REQUEST_UPDDATE")));
        o.setVchHssRequestCmd(rs.getString("VCH_HSS_REQUEST_CMD"));

        if (rs.getClob("VCH_HSS_REQUEST_CMDRESPONSE") != null) {
            Clob clob = rs.getClob("VCH_HSS_REQUEST_CMDRESPONSE");
            o.setVchHssRequestCmdresponse(clob.getSubString(1, (int) clob.length()));
        }

        o.setFecHssRequestCmdsenddate(DateUtil.convertDate(rs.getDate("FEC_HSS_REQUEST_CMDSENDDATE")));
        o.setNumHssRequestStatus(rs.getInt("NUM_HSS_REQUEST_STATUS"));
        o.setVchHssRequestMessage(rs.getString("VCH_HSS_REQUEST_MESSAGE"));
        o.setNumHssRequestRetry(rs.getInt("NUM_HSS_REQUEST_RETRY"));

        if (rs.getTimestamp("FEC_HSS_REQUEST_DEFERREDDATE") != null) {
            o.setFecHssRequestDeferreddate(DateUtil.convertDate(rs.getTimestamp("FEC_HSS_REQUEST_DEFERREDDATE")));
        }

        return o;
    }
}

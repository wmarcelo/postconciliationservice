package pe.com.entel.hss.exception;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class ImeiNotFoundException extends Exception {

    public ImeiNotFoundException(Exception ex) {
        super(ex);
    }

    public ImeiNotFoundException(String message) {
        super(message);
    }

}

package pe.com.entel.hss.exception;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class ValueNotFoundException extends Exception {

    public ValueNotFoundException(Exception ex) {
        super(ex);
    }

    public ValueNotFoundException(String message) {
        super(message);
    }

}

package pe.com.entel.hss.exception;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class ImsiNotFoundException extends Exception {

    public ImsiNotFoundException(Exception ex) {
        super(ex);
    }

    public ImsiNotFoundException(String message) {
        super(message);
    }

}

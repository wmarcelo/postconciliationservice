package pe.com.entel.hss.exception;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class RepositoryException extends Exception {

    public RepositoryException(Exception ex) {
        super(ex);
    }

    public RepositoryException(String message) {
        super(message);
    }

}

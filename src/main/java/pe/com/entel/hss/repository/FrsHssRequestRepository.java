package pe.com.entel.hss.repository;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.com.entel.hss.domain.FrsHssRequest;
import pe.com.entel.hss.exception.RepositoryException;
import pe.com.entel.hss.mapper.FrsHssRequestMapper;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@Repository
public class FrsHssRequestRepository {

    private static Logger logger = Logger.getLogger(FrsHssRequestRepository.class);

    @Autowired
    @Qualifier("hssDataSource")
    private DataSource ds;

    @Value("${hss.bd.schema}")
    private String schema;

    @Value("${hss.bd.package}")
    private String mainPackage;

    public List<FrsHssRequest> getByMaxRegAndStatus(Integer maxRegs, Integer status) throws RepositoryException {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_GETREQUEST");

            jdbcCall.addDeclaredParameter(new SqlParameter("ANUM_MAXREG", OracleTypes.NUMERIC));
            jdbcCall.addDeclaredParameter(new SqlParameter("ANUM_STATUS", OracleTypes.NUMERIC));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("ACUR_REQUESTS",
                    OracleTypes.CURSOR, new FrsHssRequestMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            SqlParameterSource sqlParams = new MapSqlParameterSource()
                    .addValue("ANUM_MAXREG", maxRegs)
                    .addValue("ANUM_STATUS", status);

            Map<String, Object> result = jdbcCall.execute(sqlParams);
            String message = (String) result.get("AVCH_MESSAGE");
            if (message != null) {
                throw new RepositoryException(message);
            }

            List<FrsHssRequest> modeList = (List<FrsHssRequest>) result.get("ACUR_REQUESTS");

            if (modeList == null || modeList.isEmpty()) {
                modeList = new ArrayList<FrsHssRequest>();
            }

            return modeList;
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    public void update(FrsHssRequest frsHssRequest) throws RepositoryException {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_UPD_REQUEST");

            jdbcCall.addDeclaredParameter(new SqlParameter("ATO_REQUEST", OracleTypes.STRUCT, frsHssRequest.getSQLTypeName()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            SqlParameterSource sqlParams = new MapSqlParameterSource()
                    .addValue("ATO_REQUEST", frsHssRequest);

            Map<String, Object> result = jdbcCall.execute(sqlParams);
            String message = (String) result.get("AVCH_MESSAGE");

            if (message != null) {
                throw new RepositoryException(message);
            }
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    public void moveBlackListToRquest() throws RepositoryException {
        try {

            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_LOADHSSREQUEST");

            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            Map<String, Object> result = jdbcCall.execute();
            String message = (String) result.get("AVCH_MESSAGE");

            if (message != null) {
                logger.info("No se movieron los registros");
                throw new RepositoryException(message);
            }

            logger.info("Se movieron todos los registros");

        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    public void moveHistoricalRecords() throws RepositoryException {
        try {

            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_LOADHISTBLACKLIST");

            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            Map<String, Object> result = jdbcCall.execute();
            String message = (String) result.get("AVCH_MESSAGE");

            if (message != null) {
                logger.info("No se movieron los registros");
                throw new RepositoryException(message);
            }

            logger.info("Se movieron todos los registros");

        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }


}



package pe.com.entel.hss.repository;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.com.entel.hss.exception.RepositoryException;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@Repository
public class GetEmailReportRepository {

    private static Logger logger = Logger.getLogger(FrsHssRequestRepository.class);

    @Autowired
    @Qualifier("hssDataSource")
    private DataSource ds;

    @Value("${hss.bd.schema}")
    private String schema;

    @Value("${hss.bd.package}")
    private String mainPackage;

    public String getEmailReport(String ddmmyyyy) throws RepositoryException {
        try {

            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_GETSTATUSREPORT");

            jdbcCall.addDeclaredParameter(new SqlParameter("AVCH_FECDATE", OracleTypes.VARCHAR));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_STATUS", OracleTypes.VARCHAR));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            SqlParameterSource sqlParams = new MapSqlParameterSource()
                    .addValue("AVCH_FECDATE", ddmmyyyy);

            Map<String, Object> result = jdbcCall.execute(sqlParams);

            String statusReport = (String) result.get("AVCH_STATUS");
            String message = (String) result.get("AVCH_MESSAGE");

            if (message != null) {
                throw new RepositoryException(message);
            }

            return statusReport;

        } catch (Exception e) {
            throw new RepositoryException(e);
        }

    }

}

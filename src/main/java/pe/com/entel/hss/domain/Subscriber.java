package pe.com.entel.hss.domain;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@XmlRootElement(name="subscriber")
@XmlAccessorType(XmlAccessType.NONE)
public class Subscriber {
    @XmlElement(name="msisdn")
    private String msisdn;
    @XmlElement(name="imsi")
    private String imsi;

    public Subscriber(){}

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }
}
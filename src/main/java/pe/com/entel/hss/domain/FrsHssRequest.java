package pe.com.entel.hss.domain;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
public class FrsHssRequest implements Serializable, SQLData {

    public static final Integer STATUS_REGISTERED = 2;

    public static final Integer STATUS_SENT = 1;

    public static final Integer STATUS_OK = 0;

    public static final Integer STATUS_ERROR = -1;

    public static final Integer STATUS_RETRY = -2;

    private String sql_type;

    private Integer numHssRequestId;

    private Integer numIdEirHistoricBl;

    private Date fecModEirHistoricBl;

    private String vchHssRequestImei;

    private String vchHssRequestImsi;

    private Date fecHssRequestEntdate;

    private Date fecHssRequestUpddate;

    private String vchHssRequestCmd;

    private String vchHssRequestCmdresponse;

    private Date fecHssRequestCmdsenddate;

    private Date fecHssRequestCmdrespdate;

    private Integer numHssRequestStatus;

    private String vchHssRequestMessage;

    private Integer numHssRequestRetry;

    private Date fecHssRequestDeferreddate;

    public Integer getNumHssRequestId() {
        return numHssRequestId;
    }

    public void setNumHssRequestId(Integer numHssRequestId) {
        this.numHssRequestId = numHssRequestId;
    }

    public Integer getNumIdEirHistoricBl() {
        return numIdEirHistoricBl;
    }

    public void setNumIdEirHistoricBl(Integer numIdEirHistoricBl) {
        this.numIdEirHistoricBl = numIdEirHistoricBl;
    }

    public Date getFecModEirHistoricBl() {
        return fecModEirHistoricBl;
    }

    public void setFecModEirHistoricBl(Date fecModEirHistoricBl) {
        this.fecModEirHistoricBl = fecModEirHistoricBl;
    }

    public String getVchHssRequestImei() {
        return vchHssRequestImei;
    }

    public void setVchHssRequestImei(String vchHssRequestImei) {
        this.vchHssRequestImei = vchHssRequestImei;
    }

    public String getVchHssRequestImsi() {
        return vchHssRequestImsi;
    }

    public void setVchHssRequestImsi(String vchHssRequestImsi) {
        this.vchHssRequestImsi = vchHssRequestImsi;
    }

    public Date getFecHssRequestEntdate() {
        return fecHssRequestEntdate;
    }

    public void setFecHssRequestEntdate(Date fecHssRequestEntdate) {
        this.fecHssRequestEntdate = fecHssRequestEntdate;
    }

    public Date getFecHssRequestUpddate() {
        return fecHssRequestUpddate;
    }

    public void setFecHssRequestUpddate(Date fecHssRequestUpddate) {
        this.fecHssRequestUpddate = fecHssRequestUpddate;
    }

    public String getVchHssRequestCmd() {
        return vchHssRequestCmd;
    }

    public void setVchHssRequestCmd(String vchHssRequestCmd) {
        this.vchHssRequestCmd = vchHssRequestCmd;
    }

    public String getVchHssRequestCmdresponse() {
        return vchHssRequestCmdresponse;
    }

    public void setVchHssRequestCmdresponse(String vchHssRequestCmdresponse) {
        this.vchHssRequestCmdresponse = vchHssRequestCmdresponse;
    }

    public Date getFecHssRequestCmdsenddate() {
        return fecHssRequestCmdsenddate;
    }

    public void setFecHssRequestCmdsenddate(Date fecHssRequestCmdsenddate) {
        this.fecHssRequestCmdsenddate = fecHssRequestCmdsenddate;
    }

    public Integer getNumHssRequestStatus() {
        return numHssRequestStatus;
    }

    public void setNumHssRequestStatus(Integer numHssRequestStatus) {
        this.numHssRequestStatus = numHssRequestStatus;
    }

    public String getVchHssRequestMessage() {
        return vchHssRequestMessage;
    }

    public void setVchHssRequestMessage(String vchHssRequestMessage) {
        this.vchHssRequestMessage = vchHssRequestMessage;
    }

    public Integer getNumHssRequestRetry() {
        return numHssRequestRetry;
    }

    public void setNumHssRequestRetry(Integer numHssRequestRetry) {
        this.numHssRequestRetry = numHssRequestRetry;
    }

    public Date getFecHssRequestDeferreddate() {
        return fecHssRequestDeferreddate;
    }

    public void setFecHssRequestDeferreddate(Date fecHssRequestDeferreddate) {
        this.fecHssRequestDeferreddate = fecHssRequestDeferreddate;
    }

    public Date getFecHssRequestCmdrespdate() {
        return fecHssRequestCmdrespdate;
    }

    public void setFecHssRequestCmdrespdate(Date fecHssRequestCmdrespdate) {
        this.fecHssRequestCmdrespdate = fecHssRequestCmdrespdate;
    }

    @Override
    public String toString() {
        return "FrsHssRequest{" +
                "numHssRequestId=" + numHssRequestId +
                ", numIdEirHistoricBl=" + numIdEirHistoricBl +
                ", fecModEirHistoricBl=" + fecModEirHistoricBl +
                ", vchHssRequestImei='" + vchHssRequestImei + '\'' +
                ", vchHssRequestImsi='" + vchHssRequestImsi + '\'' +
                ", fecHssRequestEntdate=" + fecHssRequestEntdate +
                ", fecHssRequestUpddate=" + fecHssRequestUpddate +
                ", vchHssRequestCmd='" + vchHssRequestCmd + '\'' +
                ", vchHssRequestCmdresponse='" + vchHssRequestCmdresponse + '\'' +
                ", fecHssRequestCmdsenddate=" + fecHssRequestCmdsenddate +
                ", numHssRequestStatus=" + numHssRequestStatus +
                ", vchHssRequestMessage='" + vchHssRequestMessage + '\'' +
                ", numHssRequestRetry=" + numHssRequestRetry +
                ", fecHssRequestDeferreddate=" + fecHssRequestDeferreddate +
                '}';
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "EIR.TO_FRS_HSS_REQUEST";
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        sql_type = typeName;
        this.
                setNumHssRequestRetry(stream.readInt());
        setNumIdEirHistoricBl(stream.readInt());

        if (stream.readDate() != null) {
            setFecModEirHistoricBl(new Date(stream.readDate().getTime()));
        }

        setVchHssRequestImei(stream.readString());
        setVchHssRequestImsi(stream.readString());

        if (stream.readDate() != null) {
            setFecHssRequestEntdate(new Date(stream.readDate().getTime()));
        }

        if (stream.readDate() != null) {
            setFecHssRequestUpddate(new Date(stream.readDate().getTime()));
        }

        setVchHssRequestCmd(stream.readString());

        if (stream.readString() != null) {
            setVchHssRequestCmdresponse(stream.readString());
        }

        if (stream.readDate() != null) {
            setFecHssRequestCmdsenddate(new Date(stream.readDate().getTime()));
        }

        if (stream.readDate() != null) {
            setFecHssRequestCmdrespdate(new Date(stream.readDate().getTime()));
        }

        setNumHssRequestStatus(stream.readInt());
        setVchHssRequestMessage(stream.readString());
        setNumHssRequestRetry(stream.readInt());

        if (stream.readTimestamp() != null) {
            setFecHssRequestDeferreddate(new Date(stream.readTimestamp().getTime()));
        }

    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeLong(getNumHssRequestId());
        stream.writeInt(getNumIdEirHistoricBl());

        if (getFecModEirHistoricBl() != null) {
            stream.writeDate(new java.sql.Date(getFecModEirHistoricBl().getTime()));
        } else {
            stream.writeDate(null);
        }

        stream.writeString(getVchHssRequestImei());
        stream.writeString(getVchHssRequestImsi());

        if (getFecHssRequestEntdate() != null) {
            stream.writeDate(new java.sql.Date(getFecHssRequestEntdate().getTime()));
        } else {
            stream.writeDate(null);
        }

        if (getFecHssRequestUpddate() != null) {
            stream.writeDate(new java.sql.Date(getFecHssRequestUpddate().getTime()));
        } else {
            stream.writeDate(null);
        }

        stream.writeString(getVchHssRequestCmd());

        if (getVchHssRequestCmdresponse() != null) {
            stream.writeString(getVchHssRequestCmdresponse());
        } else {
            stream.writeString(null);
        }

        if (getFecHssRequestCmdsenddate() != null) {
            stream.writeDate(new java.sql.Date(getFecHssRequestCmdsenddate().getTime()));
        } else {
            stream.writeDate(null);
        }

        if (getFecHssRequestCmdrespdate() != null) {
            stream.writeDate(new java.sql.Date(getFecHssRequestCmdrespdate().getTime()));
        } else {
            stream.writeDate(null);
        }

        stream.writeInt(getNumHssRequestStatus());
        stream.writeString(getVchHssRequestMessage());
        stream.writeInt(getNumHssRequestRetry());

        if (getFecHssRequestDeferreddate() != null) {
            stream.writeTimestamp(new java.sql.Timestamp(getFecHssRequestDeferreddate().getTime()));
        } else {
            stream.writeTimestamp(null);
        }
    }
}

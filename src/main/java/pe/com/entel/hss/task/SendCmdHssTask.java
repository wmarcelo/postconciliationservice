package pe.com.entel.hss.task;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pe.com.entel.hss.constant.HSSServiceConstants;
import pe.com.entel.hss.domain.FrsHssRequest;
import pe.com.entel.hss.domain.FrsRequestImsi;
import pe.com.entel.hss.exception.ImeiNotFoundException;
import pe.com.entel.hss.exception.ImsiNotFoundException;
import pe.com.entel.hss.repository.FrsHssRequestRepository;
import pe.com.entel.hss.repository.GetImsiRepository;
import pe.com.entel.hss.util.TelnetClientHandler;
import pe.com.entel.hss.util.TelnetClientHandlerPool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static pe.com.entel.hss.constant.HSSServiceConstants.HSS_HOST;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@Component
@Scope("prototype")
public class SendCmdHssTask implements Runnable {

    private static final Logger logger = Logger.getLogger(SendCmdHssTask.class);

    @Autowired
    private FrsHssRequestRepository frsHssRequestRepository;

    @Autowired
    private GetImsiRepository getImsiRepository;

    private FrsHssRequest frsHssRequest;

    private CountDownLatch countDownLatch;

    private TelnetClientHandler.TelnetClientHandlerBuilder telnetEIRClientHandlerBuilder;

    private String commandFormat;

    private String commandResSucc;

    private String commandResErr;

    private String cmdEnd;

    private Integer maxRetry;

    private Integer timeToReproc;

    private FrsRequestImsi frsRequestImsi;

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private StringBuffer cmdResponseStr;

    @Override
    public void run() {
        TelnetClientHandler clientHss = null;

        try {
            logger.info("Inicia ejecucion request -> " + frsHssRequest.getNumHssRequestId());

            if (StringUtils.isBlank(frsHssRequest.getVchHssRequestImei())) {
                throw new ImeiNotFoundException("IMEI es nulo o blanco");
            }
            logger.debug("Imei obtenido: " + frsHssRequest.getVchHssRequestImei());

            String imsi = getImsifromNortBound(frsHssRequest.getVchHssRequestImei());

            if (StringUtils.isBlank(imsi)) {
                imsi = getImsiRepository.getImsiFromDMS(frsRequestImsi.getDmsWsUrl(), frsHssRequest.getVchHssRequestImei());
                if (StringUtils.isBlank(imsi)) {
                    throw new ImsiNotFoundException("No se encontro IMSI registrado para IMEI: " +
                            frsHssRequest.getVchHssRequestImei());
                }
            }

            logger.info("Request -> [ " + frsHssRequest.getNumHssRequestId() +
                    " ], IMEI -> [ " + frsHssRequest.getVchHssRequestImei() +
                    " ], IMSI -> [ " + imsi + " ]");

            String command = String.format(commandFormat, imsi);

            clientHss = telnetEIRClientHandlerBuilder.createTelnetClientHandler();

            clientHss.setCmdEnd(cmdEnd);
            clientHss.setCmdErr(commandResErr);
            clientHss.setCmdVal(commandResSucc);

            clientHss.open();

            frsHssRequest.setVchHssRequestImsi(imsi);
            frsHssRequest.setVchHssRequestCmd(command);
            frsHssRequest.setFecHssRequestCmdsenddate(new Date());
            frsHssRequest.setNumHssRequestStatus(FrsHssRequest.STATUS_SENT);
            frsHssRequest.setFecHssRequestUpddate(new Date());

            frsHssRequestRepository.update(frsHssRequest);

            Map<String, Object> result = clientHss.execCmd(command);
            String cmdResp = String.valueOf(result.get("resp"));
            frsHssRequest.setVchHssRequestCmdresponse(cmdResp);
            frsHssRequest.setFecHssRequestCmdrespdate(new Date());
            frsHssRequest.setFecHssRequestUpddate(new Date());

            boolean isCommandOk = (Boolean) result.get("isOk");

            if (isCommandOk) {
                logger.info("Comando ejecutado -> [ OK ]");
                frsHssRequest.setNumHssRequestStatus(FrsHssRequest.STATUS_OK);
                frsHssRequest.setVchHssRequestMessage(null);
            } else {
                logger.info("Comando ejecutado -> [ ERROR ]");
                int countRetry = maxRetry - frsHssRequest.getNumHssRequestRetry();
                logger.info(frsHssRequest.getVchHssRequestImsi() + "tiene  [ " + countRetry + " ] reintentos");
                if (countRetry <= 0) {
                    logger.debug("El registro se guardo con ERROR");
                    frsHssRequest.setNumHssRequestStatus(FrsHssRequest.STATUS_ERROR);
                } else {
                    Date retryDate = new Date(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeToReproc));
                    logger.debug("El registro se guardo con RETRY para fecha: " + retryDate);

                    String msgErr = "ERR" + StringUtils.substringBetween(cmdResp, "ERR", "--");

                    frsHssRequest.setVchHssRequestMessage(msgErr);
                    frsHssRequest.setFecHssRequestDeferreddate(retryDate);
                    frsHssRequest.setNumHssRequestStatus(FrsHssRequest.STATUS_RETRY);
                    frsHssRequest.setNumHssRequestRetry(frsHssRequest.getNumHssRequestRetry() + 1);
                }
            }

            frsHssRequestRepository.update(frsHssRequest);

        } catch (ImeiNotFoundException e) {
            logger.warn(e.getMessage());
            try {
                frsHssRequest.setNumHssRequestStatus(FrsHssRequest.STATUS_ERROR);
                frsHssRequest.setFecHssRequestUpddate(new Date());
                frsHssRequest.setVchHssRequestMessage(StringUtils.abbreviate(e.getMessage(), 1000));
                frsHssRequestRepository.update(frsHssRequest);
            } catch (Exception ex) {
                logger.error(e);
            }
        } catch (ImsiNotFoundException e) {
            logger.warn(e.getMessage());
            try {
                frsHssRequest.setNumHssRequestStatus(FrsHssRequest.STATUS_ERROR);
                frsHssRequest.setFecHssRequestUpddate(new Date());
                frsHssRequest.setVchHssRequestMessage(StringUtils.abbreviate(e.getMessage(), 1000));
                frsHssRequestRepository.update(frsHssRequest);
            } catch (Exception ex) {
                logger.error(ex);
            }
        } catch (Exception e) {
            logger.error(e);
            try {
                frsHssRequest.setNumHssRequestStatus(FrsHssRequest.STATUS_ERROR);
                frsHssRequest.setFecHssRequestUpddate(new Date());
                frsHssRequest.setVchHssRequestMessage(StringUtils.abbreviate(e.getMessage(), 1000));
                frsHssRequestRepository.update(frsHssRequest);
            } catch (Exception ex) {
                logger.error(ex);
            }
        } finally {
            if (clientHss != null) {
                try {
                    clientHss.disconnect();
                } catch (Exception e) {
                    logger.error(e);
                }
            }
            countDownLatch.countDown();
        }
    }


    public String getImsifromNortBound(String imei) throws Exception {
        String imsi = null;
        Map<String, Object> resp = null;
        TelnetClientHandler client = null;
        String cmdReq = String.format(frsRequestImsi.getRegneCmdNB(), imei);

        try {
            client = new TelnetClientHandler(frsRequestImsi.getHostNB(),
                    frsRequestImsi.getPortNB(), frsRequestImsi.getTimeoutReadNB(), frsRequestImsi.getTimeoutConnectNB());
            client.setLoginCmd(frsRequestImsi.getLogin1CmdNB());
            client.setLoginVal(frsRequestImsi.getLoginValNB());
            client.setCmdEnd("END");
            client.setCmdErr("");

            client.open();

            client.setCmdVal(frsRequestImsi.getRegneValNB());
            resp = client.execCmd(cmdReq);

            Boolean cmdIsOk = (Boolean) resp.get("isOk");

            logger.debug("Primero comando es OK? " + cmdIsOk);

            if (cmdIsOk) {
                String cmdresp = (String) resp.get("resp");
                int firstIndex = cmdresp.indexOf("IMSI");
                imsi = cmdresp.substring(firstIndex, firstIndex + HSSServiceConstants.IMSI_FINAL_POSITION);

                logger.debug("Imsi encontrado " + imsi);
                return imsi;
            }

            client.setCmdVal(frsRequestImsi.getLoginValNB());
            resp = client.execCmd(frsRequestImsi.getLogin2CmdNB());
            Boolean login2IsOk = (Boolean) resp.get("isOk");

            if (!login2IsOk) {
                throw new Exception("No login 2 Nortbound");
            }

            client.setCmdVal(frsRequestImsi.getRegneValNB());
            resp = client.execCmd(cmdReq);
            Boolean cmd2IsOk = (Boolean) resp.get("isOk");

            if (cmd2IsOk) {
                String cmdresp = (String) resp.get("resp");
                int firstIndex = cmdresp.indexOf("IMSI");
                imsi = cmdresp.substring(firstIndex, firstIndex + HSSServiceConstants.IMSI_FINAL_POSITION);
                return imsi;
            }


            client.setCmdVal(frsRequestImsi.getLoginValNB());
            resp = client.execCmd(frsRequestImsi.getLogin3CmdNB());
            Boolean login3IsOk = (Boolean) resp.get("isOk");

            if (!login3IsOk) {
                throw new Exception("No login 3 Nortbound");
            }

            client.setCmdVal(frsRequestImsi.getRegneValNB());
            resp = client.execCmd(cmdReq);

            Boolean cmd3IsOk = (Boolean) resp.get("isOk");

            if (cmd3IsOk) {
                String cmdresp = (String) resp.get("resp");
                int firstIndex = cmdresp.indexOf("IMSI");
                imsi = cmdresp.substring(firstIndex, firstIndex + HSSServiceConstants.IMSI_FINAL_POSITION);
                return imsi;
            }

            return null;

        } finally {
            try {
                if (client != null) {
                    client.disconnect();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
    }


    public void startConnection() throws IOException {
        clientSocket = new Socket(frsRequestImsi.getHostNB(), frsRequestImsi.getPortNB());
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public void loginNB(String loginCmdNB) {
        logger.info("El comando de inicio es: " + loginCmdNB);
        out.println(loginCmdNB);
        out.flush();
        String respCommand;
        String respFormatted;
        cmdResponseStr = new StringBuffer();
        int cont = 0;
        try {
            while ((respCommand = in.readLine()) != null) {
                cmdResponseStr.append(respCommand);
                if (cont == 6) {
                    break;
                }
                cont++;
            }
            respFormatted = cmdResponseStr.toString();
            if (respFormatted.contains("Success")) {
                logger.info("Logueado!!!");
            } else {
                logger.info("fallo el logueo!!!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getImsi(String imei) throws IOException {
        String commandGetImsi = frsRequestImsi.getRegneCmdNB().replace("%s", imei);
        logger.info("El comando a ejecutar es el siguiente: " + commandGetImsi);
        String imsi;
        out.println(commandGetImsi);
        String respCommand;
        String respFormatted;
        cmdResponseStr = new StringBuffer();
        int cont = 0;
        while ((respCommand = in.readLine()) != null) {
            cmdResponseStr.append(respCommand);
            if (cont == 6) {
                break;
            }
            cont++;
        }
        respFormatted = cmdResponseStr.toString();
        if (respFormatted.contains("Operation succeeded")) {
            logger.info("Se envio el comando!!!");
        } else {
            logger.info("No se envio el comando!!!");
        }
        return "OK";
    }

    public void logoutNB(String logoutCmdNB) {
        logger.info("El comando de logout es: " + logoutCmdNB);
        out.println(logoutCmdNB);
        String respCommand;
        String respFormatted;
        cmdResponseStr = new StringBuffer();
        try {
            int cont = 0;
            while ((respCommand = in.readLine()) != null) {
                cmdResponseStr.append(respCommand);
                if (cont == 5) {
                    break;
                }
                cont++;
            }
            respFormatted = cmdResponseStr.toString();
            if (respFormatted.contains("Success")) {
                logger.info("Deslogueado!!!");
            } else {
                logger.info("fallo el deslogueo!!!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public void setFrsHssRequest(FrsHssRequest frsHssRequest) {
        this.frsHssRequest = frsHssRequest;
    }

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public String getCommandFormat() {
        return commandFormat;
    }

    public void setCommandFormat(String commandFormat) {
        this.commandFormat = commandFormat;
    }

    public String getCommandResSucc() {
        return commandResSucc;
    }

    public void setCommandResSucc(String commandResSucc) {
        this.commandResSucc = commandResSucc;
    }

    public String getCommandResErr() {
        return commandResErr;
    }

    public void setCommandResErr(String commandResErr) {
        this.commandResErr = commandResErr;
    }

    public FrsHssRequest getFrsHssRequest() {
        return frsHssRequest;
    }

    public TelnetClientHandler.TelnetClientHandlerBuilder getTelnetEIRClientHandlerBuilder() {
        return telnetEIRClientHandlerBuilder;
    }

    public void setTelnetEIRClientHandlerBuilder(TelnetClientHandler.TelnetClientHandlerBuilder telnetEIRClientHandlerBuilder) {
        this.telnetEIRClientHandlerBuilder = telnetEIRClientHandlerBuilder;
    }

    public String getCmdEnd() {
        return cmdEnd;
    }

    public void setCmdEnd(String cmdEnd) {
        this.cmdEnd = cmdEnd;
    }

    public Integer getMaxRetry() {
        return maxRetry;
    }

    public void setMaxRetry(Integer maxRetry) {
        this.maxRetry = maxRetry;
    }

    public Integer getTimeToReproc() {
        return timeToReproc;
    }

    public void setTimeToReproc(Integer timeToReproc) {
        this.timeToReproc = timeToReproc;
    }

    public FrsRequestImsi getFrsRequestImsi() {
        return frsRequestImsi;
    }

    public void setFrsRequestImsi(FrsRequestImsi frsRequestImsi) {
        this.frsRequestImsi = frsRequestImsi;
    }
}

package pe.com.entel.hss.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.entel.hss.repository.FrsHssRequestRepository;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@Service
public class HSSMoveBlackListToRequest {

    static final Logger logger = Logger.getLogger(HSSMoveBlackListToRequest.class);

    @Autowired
    private FrsHssRequestRepository frsHssRequestRepository;

    public void exec() throws Exception {
        try {
            logger.info(">>>> Servicio de mover BLACKLIST hacia REQUEST <<<<");

            logger.info("Moviendo registros de BLACKLIST a REQUEST ... ");
            frsHssRequestRepository.moveBlackListToRquest();
            logger.info("Servicio OK!");

        } catch (Exception e) {
            logger.error(e);
        } finally {
            logger.info(">>>> Fin Servicio de mover BLACKLIST hacia REQUEST <<<<");
        }
    }

}


package pe.com.entel.hss.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.entel.hss.domain.FrsHssRequestCfg;
import pe.com.entel.hss.exception.RepositoryException;
import pe.com.entel.hss.repository.FrsHssRequestCfgRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@Component
public class HSSInitialConfig {

    private Logger logger = Logger.getLogger(HSSInitialConfig.class);

    @Autowired
    private FrsHssRequestCfgRepository frsHssRequestCfgRepository;

    private Map<String, Object> paramInMemory = new HashMap<String, Object>();
    private Map<String, FrsHssRequestCfg> paramInMemoryBean = new HashMap<String, FrsHssRequestCfg>();

    public void loadParameter() throws RepositoryException {
        logger.info(">>>> Cargando parametros iniciales de BD ... <<<<");

        logger.debug("frsHssRequestCfgRepository: " + frsHssRequestCfgRepository);
        List<FrsHssRequestCfg> paramList = frsHssRequestCfgRepository.getAll();

        for (FrsHssRequestCfg param : paramList) {
            logger.info("Parametro [ " + param.getVchHssKey() + " ] => [ " + param.getVchHssValue() + " ]");
            paramInMemory.put(param.getVchHssKey(),
                    paramConverted(param.getVchHssValue(), param.getVchHssDatatype()));
            paramInMemoryBean.put(param.getVchHssKey(), param);
        }

        logger.info(">>>> Fin de la carga <<<<");
    }

    private Object paramConverted(String value, String dataType) {
        if ("NUMBER".equalsIgnoreCase(dataType)) {
            Integer n = 0;
            try {
                n = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                logger.warn(e);
                n = 0;
            }
            return n;
        } else {
            return value;
        }
    }

    public FrsHssRequestCfg geParam(String key) {
        return paramInMemoryBean.get(key);
    }

    public Integer getIntParam(String key) {
        return (Integer) paramInMemory.get(key);
    }

    public String getStrParam(String key) {
        return (String) paramInMemory.get(key);
    }

    public FrsHssRequestCfgRepository getFrsHssRequestCfgRepository() {
        return frsHssRequestCfgRepository;
    }

    public void setFrsHssRequestCfgRepository(FrsHssRequestCfgRepository frsHssRequestCfgRepository) {
        this.frsHssRequestCfgRepository = frsHssRequestCfgRepository;
    }
}

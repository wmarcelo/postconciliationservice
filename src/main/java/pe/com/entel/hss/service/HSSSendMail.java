package pe.com.entel.hss.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.entel.hss.domain.FrsHssRequestCfg;
import pe.com.entel.hss.exception.ValueNotFoundException;
import pe.com.entel.hss.repository.FrsHssRequestCfgRepository;
import pe.com.entel.hss.repository.GetEmailReportRepository;
import pe.com.entel.hss.util.SenderEmailUtil;
import pe.com.entel.hss.ws.EnviarEmailResponse;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static pe.com.entel.hss.constant.HSSServiceConstants.*;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@Service
public class HSSSendMail {

    private static Logger logger = Logger.getLogger(HSSSendMail.class);

    @Autowired
    private GetEmailReportRepository getEmailReportRepository;

    @Autowired
    private FrsHssRequestCfgRepository frsHssRequestCfgRepository;

    @Autowired
    private HSSInitialConfig hSSInitialConfig;

    private static final String newLineSymbol = "<br>";

    public void exec() {
        try {
            logger.info(">>>> Servicio de envio de correo <<<<");

            String flagStr = hSSInitialConfig.getStrParam(MAIL_FLAG);
            verifyValue(flagStr, MAIL_FLAG);

            if ("no".equalsIgnoreCase(flagStr)) {
                logger.warn("Flag de email se ecuentra Desactivado. No se envia email!");
                return;
            }

            Integer dayHour = hSSInitialConfig.getIntParam(MAIL_FRECUENCY);
            verifyValue(dayHour, MAIL_FRECUENCY);

            Calendar calConfig = Calendar.getInstance();
            calConfig.set(Calendar.HOUR_OF_DAY, dayHour);
            calConfig.set(Calendar.MINUTE, 0);
            calConfig.set(Calendar.SECOND, 0);

            Calendar calSystem = Calendar.getInstance();
            calSystem.set(Calendar.MINUTE, 0);
            calSystem.set(Calendar.SECOND, 0);

            if (calSystem.compareTo(calConfig) != 0) {
                logger.warn("No se ejecuto debido a que no es la hora programada!");
                return;
            }

            FrsHssRequestCfg beanCfg = hSSInitialConfig.geParam(MAIL_FRECUENCY);
            beanCfg.getFecHssUpddate();

            if (beanCfg.getFecHssUpddate() != null) {
                logger.debug("Se verifica que se ha ejecutado ");
                Calendar lastEjecCal = Calendar.getInstance();
                lastEjecCal.setTime(beanCfg.getFecHssUpddate());

                if (lastEjecCal.get(Calendar.DAY_OF_MONTH) != calSystem.get(Calendar.DAY_OF_MONTH)) {
                    logger.debug("Cambio de dia");
                    sendMail(beanCfg);
                } else {
                    logger.warn("Ya ha sido ejecutado una vez en el dia!");
                }
            } else {
                sendMail(beanCfg);
            }

        } catch (Exception e) {
            logger.error(e);
        } finally {
            logger.info(">>>> Fin Servicio de envio de correo <<<<");
        }
    }

    private void sendMail(FrsHssRequestCfg beanCfg) throws Exception {
        try {
            String serviceUrl = hSSInitialConfig.getStrParam(MAIL_SERVICE_URL);
            String from = hSSInitialConfig.getStrParam(MAIL_FROM);
            List<String> to = (Arrays.asList(StringUtils.splitByWholeSeparator(hSSInitialConfig.getStrParam(MAIL_TO), ";")));
            String subject = hSSInitialConfig.getStrParam(MAIL_SUBJECT);
            String preMessage = hSSInitialConfig.getStrParam(MAIL_MESSAGE);

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat sdfCorreo = new SimpleDateFormat("dd-MM-yyyy");
            subject = String.format(subject, sdfCorreo.format(new Date()));

            String reportStatus = getEmailReportRepository.getEmailReport(sdf.format(new Date()));

            StringBuilder stringBuilder = new StringBuilder();

            //CONTRUCCION MENSAJE
            stringBuilder.append("Buenos d\u00edas," + newLineSymbol);
            stringBuilder.append(newLineSymbol);
            stringBuilder.append(preMessage + newLineSymbol);
            stringBuilder.append(newLineSymbol);
            stringBuilder.append(reportStatus.replace(",", newLineSymbol) + newLineSymbol);
            stringBuilder.append(newLineSymbol);

            String message = stringBuilder.toString();

            logger.info("Mensaje -> " + message.replace(newLineSymbol, System.getProperty("line.separator")));

            EnviarEmailResponse response = SenderEmailUtil.send(serviceUrl, from, to, subject, message);

            if (response != null && RESPONSE_WS_MAIL_OK.equals(response.getAuditResponse().getErrorCode())) {
                logger.info("El mail se envio correctamente a los destinatrios -> " + hSSInitialConfig.getStrParam(MAIL_TO));

                beanCfg.setFecHssUpddate(new Date());
                frsHssRequestCfgRepository.update(beanCfg);
                logger.info("Servicio OK!");

            } else {
                logger.error("Ocurrio un problema del WS al enviar MAIL " + response.getAuditResponse().getErrorMsg());
            }

        } catch (Exception e) {
            logger.error(e);
        }

    }

    public void verifyValue(Object object, String varName) throws ValueNotFoundException {
        if (object == null) {
            throw new ValueNotFoundException(String.format("La variable %s no ha sido definido en la base de datos", varName));
        }
    }

}

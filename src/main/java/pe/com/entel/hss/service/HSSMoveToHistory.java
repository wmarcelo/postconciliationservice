package pe.com.entel.hss.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.entel.hss.domain.FrsHssRequestCfg;
import pe.com.entel.hss.exception.ValueNotFoundException;
import pe.com.entel.hss.repository.FrsHssRequestCfgRepository;
import pe.com.entel.hss.repository.FrsHssRequestRepository;

import java.util.Calendar;
import java.util.Date;

import static pe.com.entel.hss.constant.HSSServiceConstants.*;

/**
 * @version 1.0, 13/02/2019
 * @autor wmarcelo
 */
@Service
public class HSSMoveToHistory {

    static final Logger logger = Logger.getLogger(HSSMoveToHistory.class);

    @Autowired
    private FrsHssRequestCfgRepository frsHssRequestCfgRepository;

    @Autowired
    private FrsHssRequestRepository frsHssRequestRepository;

    @Autowired
    private HSSInitialConfig hSSInitialConfig;

    public void exec() {
        try {
            logger.info(">>>> Servicio de Depuracion REQUEST hacia HISTORY <<<<");
            Integer dayHour = hSSInitialConfig.getIntParam(MVHIST_FRECUENCY);
            verifyValue(dayHour, MVHIST_FRECUENCY);

            Calendar calConfig = Calendar.getInstance();
            calConfig.set(Calendar.HOUR_OF_DAY, dayHour);
            calConfig.set(Calendar.MINUTE, 0);
            calConfig.set(Calendar.SECOND, 0);

            Calendar calSystem = Calendar.getInstance();
            calSystem.set(Calendar.MINUTE, 0);
            calSystem.set(Calendar.SECOND, 0);

            if (calSystem.compareTo(calConfig) != 0) {
                logger.warn("No se ejecuto debido a que no es la hora programada!");
                return;
            }

            FrsHssRequestCfg beanCfg = hSSInitialConfig.geParam(MVHIST_FRECUENCY);
            beanCfg.getFecHssUpddate();

            if (beanCfg.getFecHssUpddate() != null) {
                logger.info("Se verifica que se ha ejecutado ");
                Calendar lastEjecCal = Calendar.getInstance();
                lastEjecCal.setTime(beanCfg.getFecHssUpddate());

                if (lastEjecCal.get(Calendar.DAY_OF_MONTH) != calSystem.get(Calendar.DAY_OF_MONTH)) {
                    logger.info("Cambio de dia");
                    moveToHistory(beanCfg);
                } else {
                    logger.warn("Ya ha sido ejecutado una vez en el dia!");
                }
            } else {
                moveToHistory(beanCfg);
            }

        } catch (Exception e) {
            logger.error(e);
        } finally {
            logger.info(">>>> Fin de Servicio de Depuracion REQUEST hacia HISTORY <<<<");
        }
    }

    private void moveToHistory(FrsHssRequestCfg beanCfg) throws Exception {
        logger.info("Moviendo registros de REQUEST hacia a HISTORICO ... ");
        frsHssRequestRepository.moveHistoricalRecords();

        beanCfg.setFecHssUpddate(new Date());
        frsHssRequestCfgRepository.update(beanCfg);
        logger.info("Servicio OK!");

    }


    public void verifyValue(Object object, String varName) throws ValueNotFoundException {
        if (object == null) {
            throw new ValueNotFoundException(String.format("La variable %s no ha sido definido en la base de datos", varName));
        }
    }

}

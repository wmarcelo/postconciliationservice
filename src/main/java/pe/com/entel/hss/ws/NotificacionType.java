
package pe.com.entel.hss.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NotificacionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NotificacionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="asunto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="texto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="importancia" type="{http://entel.com.pe/integracion/notificar/schema}ImportanciaType"/>
 *         &lt;element name="listaDestinos">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="listaDestinosCc">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="listaDestinosBcc">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="listaAdjunto">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="adjunto" type="{http://entel.com.pe/integracion/notificar/schema}ArchivoType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotificacionType", namespace = "http://entel.com.pe/integracion/notificar/schema", propOrder = {
    "origen",
    "asunto",
    "texto",
    "importancia",
    "listaDestinos",
    "listaDestinosCc",
    "listaDestinosBcc",
    "listaAdjunto"
})
public class NotificacionType {

    @XmlElement(required = true)
    protected String origen;
    @XmlElement(required = true)
    protected String asunto;
    @XmlElement(required = true)
    protected String texto;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ImportanciaType importancia;
    @XmlElement(required = true)
    protected ListaDestinos listaDestinos;
    @XmlElement(required = true)
    protected ListaDestinosCc listaDestinosCc;
    @XmlElement(required = true)
    protected ListaDestinosBcc listaDestinosBcc;
    @XmlElement(required = true)
    protected ListaAdjunto listaAdjunto;

    /**
     * Gets the value of the origen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Sets the value of the origen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

    /**
     * Gets the value of the asunto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsunto() {
        return asunto;
    }

    /**
     * Sets the value of the asunto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsunto(String value) {
        this.asunto = value;
    }

    /**
     * Gets the value of the texto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTexto() {
        return texto;
    }

    /**
     * Sets the value of the texto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTexto(String value) {
        this.texto = value;
    }

    /**
     * Gets the value of the importancia property.
     * 
     * @return
     *     possible object is
     *     {@link ImportanciaType }
     *     
     */
    public ImportanciaType getImportancia() {
        return importancia;
    }

    /**
     * Sets the value of the importancia property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportanciaType }
     *     
     */
    public void setImportancia(ImportanciaType value) {
        this.importancia = value;
    }

    /**
     * Gets the value of the listaDestinos property.
     * 
     * @return
     *     possible object is
     *     {@link ListaDestinos }
     *     
     */
    public ListaDestinos getListaDestinos() {
        return listaDestinos;
    }

    /**
     * Sets the value of the listaDestinos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaDestinos }
     *     
     */
    public void setListaDestinos(ListaDestinos value) {
        this.listaDestinos = value;
    }

    /**
     * Gets the value of the listaDestinosCc property.
     * 
     * @return
     *     possible object is
     *     {@link ListaDestinosCc }
     *     
     */
    public ListaDestinosCc getListaDestinosCc() {
        return listaDestinosCc;
    }

    /**
     * Sets the value of the listaDestinosCc property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaDestinosCc }
     *     
     */
    public void setListaDestinosCc(ListaDestinosCc value) {
        this.listaDestinosCc = value;
    }

    /**
     * Gets the value of the listaDestinosBcc property.
     * 
     * @return
     *     possible object is
     *     {@link ListaDestinosBcc }
     *     
     */
    public ListaDestinosBcc getListaDestinosBcc() {
        return listaDestinosBcc;
    }

    /**
     * Sets the value of the listaDestinosBcc property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaDestinosBcc }
     *     
     */
    public void setListaDestinosBcc(ListaDestinosBcc value) {
        this.listaDestinosBcc = value;
    }

    /**
     * Gets the value of the listaAdjunto property.
     * 
     * @return
     *     possible object is
     *     {@link ListaAdjunto }
     *     
     */
    public ListaAdjunto getListaAdjunto() {
        return listaAdjunto;
    }

    /**
     * Sets the value of the listaAdjunto property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaAdjunto }
     *     
     */
    public void setListaAdjunto(ListaAdjunto value) {
        this.listaAdjunto = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="adjunto" type="{http://entel.com.pe/integracion/notificar/schema}ArchivoType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "adjunto"
    })
    public static class ListaAdjunto {

        @XmlElement(namespace = "http://entel.com.pe/integracion/notificar/schema", required = true)
        protected List<ArchivoType> adjunto;

        /**
         * Gets the value of the adjunto property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the adjunto property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdjunto().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ArchivoType }
         * 
         * 
         */
        public List<ArchivoType> getAdjunto() {
            if (adjunto == null) {
                adjunto = new ArrayList<ArchivoType>();
            }
            return this.adjunto;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destino"
    })
    public static class ListaDestinos {

        @XmlElement(namespace = "http://entel.com.pe/integracion/notificar/schema", required = true)
        protected List<String> destino;

        /**
         * Gets the value of the destino property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the destino property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDestino().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getDestino() {
            if (destino == null) {
                destino = new ArrayList<String>();
            }
            return this.destino;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destino"
    })
    public static class ListaDestinosBcc {

        @XmlElement(namespace = "http://entel.com.pe/integracion/notificar/schema", required = true)
        protected List<String> destino;

        /**
         * Gets the value of the destino property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the destino property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDestino().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getDestino() {
            if (destino == null) {
                destino = new ArrayList<String>();
            }
            return this.destino;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destino"
    })
    public static class ListaDestinosCc {

        @XmlElement(namespace = "http://entel.com.pe/integracion/notificar/schema", required = true)
        protected List<String> destino;

        /**
         * Gets the value of the destino property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the destino property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDestino().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getDestino() {
            if (destino == null) {
                destino = new ArrayList<String>();
            }
            return this.destino;
        }

    }

}

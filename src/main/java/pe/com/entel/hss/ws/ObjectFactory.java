
package pe.com.entel.hss.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.com.entel.hss.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.com.entel.hss.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NotificacionType }
     * 
     */
    public NotificacionType createNotificacionType() {
        return new NotificacionType();
    }

    /**
     * Create an instance of {@link EnviarEmailRequest }
     * 
     */
    public EnviarEmailRequest createEnviarEmailRequest() {
        return new EnviarEmailRequest();
    }

    /**
     * Create an instance of {@link EnviarEmailResponse }
     * 
     */
    public EnviarEmailResponse createEnviarEmailResponse() {
        return new EnviarEmailResponse();
    }

    /**
     * Create an instance of {@link AuditResponseType }
     * 
     */
    public AuditResponseType createAuditResponseType() {
        return new AuditResponseType();
    }

    /**
     * Create an instance of {@link ResponseNotificarFault }
     * 
     */
    public ResponseNotificarFault createResponseNotificarFault() {
        return new ResponseNotificarFault();
    }

    /**
     * Create an instance of {@link EnviarEmailAsyncRequest }
     * 
     */
    public EnviarEmailAsyncRequest createEnviarEmailAsyncRequest() {
        return new EnviarEmailAsyncRequest();
    }

    /**
     * Create an instance of {@link ArchivoType }
     * 
     */
    public ArchivoType createArchivoType() {
        return new ArchivoType();
    }

    /**
     * Create an instance of {@link NotificacionType.ListaDestinos }
     * 
     */
    public NotificacionType.ListaDestinos createNotificacionTypeListaDestinos() {
        return new NotificacionType.ListaDestinos();
    }

    /**
     * Create an instance of {@link NotificacionType.ListaDestinosCc }
     * 
     */
    public NotificacionType.ListaDestinosCc createNotificacionTypeListaDestinosCc() {
        return new NotificacionType.ListaDestinosCc();
    }

    /**
     * Create an instance of {@link NotificacionType.ListaDestinosBcc }
     * 
     */
    public NotificacionType.ListaDestinosBcc createNotificacionTypeListaDestinosBcc() {
        return new NotificacionType.ListaDestinosBcc();
    }

    /**
     * Create an instance of {@link NotificacionType.ListaAdjunto }
     * 
     */
    public NotificacionType.ListaAdjunto createNotificacionTypeListaAdjunto() {
        return new NotificacionType.ListaAdjunto();
    }

}
